# Course 2 Calendar
This tool allows you to read in the schedule information from the [KU Leuven onderwijsaanbod website](https://onderwijsaanbod.kuleuven.be) and convert it to an iCal-format for use in e.g. Google Calendar.
This tool allows you to make `.ics`-files:
* on a **per-course** basis.
* on a **per-program** basis.
* **without** the need for an **official enrollment** in the specific course.

This tool is intended to help students pick their courses at the beginning of the year in a more interactive and clear way, when they are not yet enrolled in these courses.
This tools is **not intended** for providing up to date information on your schedule throughout the year.
There are more useful tools like [Quivr](https://quivr.be) that are made for this purpose.

## Warnings

**You use this tool at your own risk.**
This tool is hacked together quickly because I have to get my own ISP on point.

Some things don't work (yet):
* Because of Daylight Savings Time, timezone support is implemented in an awful way. Right now only Academic year 2019-2020 is fully supported.
* If the course information is not complete (no location or no course name for example) the event will be parsed incorrectly. The only solution right now is to manually edit these events in your calendar application of preference.

## Installation
This script is written in python. You are probably smart enough to figure out how to install everything. I have faith in you!

## How to use (per-course)
1. **Select a program** on [onderwijsaanbod.kuleuven.be](https://onderwijsaanbod.kuleuven.be)
2. Inside the box "**Programme Summary**" select your programme of choice.
3. **Select the course** that you want to convert to iCal-format.
4. In the course overview there is an icon with a small clock that says how many hours are devoted to this course. If you click the clock icon, a new window will pop up with a schedule of only this course.
5. Click the "**List presentation semester**"
6. **Download the HTML-source** of this page.

Now you can use the script like so:

`python3 CourseParser.py htmlsource.html 2019`

This will create a file `htmlsource.ics` that you can import in your favorite calendar application.

Other CLI options can be displayed with:

`python3 CourseParser.py --help`
